<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = factory(\App\Plan::class, 8)->create();

        $plans->each(function ($plan) {
            $days = factory(\App\PlanDay::class, rand(2, 4))->make();

            $order = 1;
            $days->each(function ($day) use (&$order, &$plan) {
                $day->order = $order++;

                $plan->days()->save($day);
            });
        });

        // Link users to plans
        $users = \App\User::all();

        $users->each(function (&$user) use (&$plans) {
            $user->plans()->attach(
                $plans->random(rand(2, 3))->pluck('id')
            );
        });
    }
}
