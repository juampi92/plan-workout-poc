import React, { Component } from 'react'

import { connect } from 'react-redux'

import PlanActions from '../actions/plan.actions'

import PlanDetails from '../components/PlanDetails'

class PlanOverviewScreen extends Component {
  renderPlan () {
    const { plan } = this.props

    if (!plan) {
      return null
    }

    return (
      <div>
        <PlanDetails plan={plan} />
      </div>
    )
  }

  render () {
    const { fetching, planName, back } = this.props

    return (<div className={`load ${fetching ? 'loading' : ''}`}>
      <a href='#' onClick={back}>{'< back'}</a>
      <div className='row'>
        <div className='col-8'>
          <h2>Plan: { planName }</h2>
          { this.renderPlan() }
        </div>
      </div>
    </div>)
  }
}

const mapDispatchToProps = dispatch => ({
  back: () => { dispatch(PlanActions.clear()) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  fetching: state.plan.get('fetching'),
  planName: state.plan.get('planId').toJS().name,
  plan: state.plan.get('plan') ? state.plan.get('plan').toJS() : null
})

export default connect(mapStateToProps, mapDispatchToProps)(PlanOverviewScreen)
