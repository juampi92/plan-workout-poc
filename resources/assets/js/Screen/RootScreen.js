import React, { Component } from 'react'

import { connect } from 'react-redux'

import Header from '../components/Header'
import BreadcrumbNavigation from '../Container/BreadcrumbNavigation'
import PlanDetailsScreen from './PlanDetailsScreen'
import PlanOverviewScreen from './PlanOverviewScreen'

import ExercisesActions from '../actions/exercises.actions'

class RootScreen extends Component {
  componentDidMount () {
    this.props.fetchExercises()
  }

  render () {
    return (
      <div style={{ marginBottom: 40 }}>
        <Header />
        <div className='container'>
          <BreadcrumbNavigation />
          { this.renderScreen() }
        </div>
      </div>
    )
  }

  renderScreen () {
    const { isPlan } = this.props

    if (isPlan) {
      return <PlanDetailsScreen />
    }

    return <PlanOverviewScreen />
  }
}

const mapDispatchToProps = dispatch => ({
  fetchExercises: () => { dispatch(ExercisesActions.fetch()) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  isPlan: state.plan.get('planId') !== null
})

export default connect(mapStateToProps, mapDispatchToProps)(RootScreen)
