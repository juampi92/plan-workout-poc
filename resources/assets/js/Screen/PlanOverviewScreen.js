import React, { Component } from 'react'

import { connect } from 'react-redux'

import PlansList from '../Container/PlansList'
import AddPlan from '../Container/AddPlan'

class PlanOverviewScreen extends Component {
  render () {
    const { fetching } = this.props

    return (<div className={`load ${fetching ? 'loading' : ''}`}>
      <div>
        <h1>Plans</h1>
      </div>
      <div className='row'>
        <div className='col-8'>
          <PlansList />
        </div>
        <div className='col-4'>
          <AddPlan />
        </div>
      </div>
    </div>)
  }
}

const mapStateToProps = (state, ownProps = {}) => ({
  fetching: state.plans.get('fetching')
})

export default connect(mapStateToProps)(PlanOverviewScreen)
