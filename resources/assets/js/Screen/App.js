import React, { Component } from 'react'
import { Provider } from 'react-redux'

import createStore from '../reducers'

import RootScreen from './RootScreen'

// create our store
const store = createStore()

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <RootScreen />
      </Provider>
    )
  }
}

export default App
