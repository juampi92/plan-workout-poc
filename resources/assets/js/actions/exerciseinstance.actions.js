import { createActions } from 'reduxsauce'
import { sharedActions } from './shared.actions'

const { Types, Creators } = createActions({
  ...sharedActions,
  // Store
  store: ['pid', 'did', 'exercise_id', 'duration', 'order'],
  push: ['pid', 'did', 'exercise'],
  // Update
  edit: ['pid', 'did', 'eid', 'exercise_id', 'duration', 'order'],
  update: ['pid', 'did', 'exercise'],
  // Delete
  delete: ['pid', 'did', 'eid'],
  remove: ['pid', 'did', 'eid']
}, {prefix: 'EXERCISE_'})

export const ExerciseInstanceTypes = Types
export default Creators
