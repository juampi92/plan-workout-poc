import { createActions } from 'reduxsauce'
import { sharedActions } from './shared.actions'

const { Types, Creators } = createActions({
  ...sharedActions,
  fetch: ['id', 'name'],
  set: ['plan'],
  clear: null,
  toggleEdit: null
}, {prefix: 'PLAN_'})

export const PlanTypes = Types
export default Creators
