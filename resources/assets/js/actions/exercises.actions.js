import { createActions } from 'reduxsauce'
import sharedActions from './shared.actions'

const { Types, Creators } = createActions({
  ...sharedActions,
  fetch: null,
  set: ['exercises']
}, {prefix: 'EXERCISES_'})

export const ExercisesTypes = Types
export default Creators
