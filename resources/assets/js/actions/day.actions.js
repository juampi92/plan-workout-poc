import { createActions } from 'reduxsauce'
import { sharedActions } from './shared.actions'

const { Types, Creators } = createActions({
  ...sharedActions,
  // Store
  store: ['pid', 'name'],
  push: ['pid', 'day'],
  // Update
  edit: ['pid', 'did', 'name'],
  update: ['pid', 'day'],
  // Delete
  delete: ['pid', 'did'],
  remove: ['pid', 'did']
}, {prefix: 'DAY_'})

export const DayTypes = Types
export default Creators
