import { createActions } from 'reduxsauce'
import sharedActions from './shared.actions'

const { Types, Creators } = createActions({
  ...sharedActions,
  fetch: null,
  set: ['plans'],
  store: ['name', 'description', 'difficulty'],
  push: ['plan'],
  edit: ['id', 'name', 'description', 'difficulty'],
  update: ['plan'],
  delete: ['id'],
  remove: ['plan']
}, {prefix: 'PLANS_'})

export const PlansTypes = Types
export default Creators
