import React, { Component } from 'react'

import UsersController from '../controllers/users.controller'
import User from './User'

export default class Users extends Component {
  constructor (props) {
    super(props)

    this.state = {
      loading: true,
      users: UsersController.users
    }
  }

  componentDidMount () {
    UsersController
      .fetch()
      .then(({ users }) => this.setState({ users, loading: false }))
  }

  render () {
    const { loading, users } = this.state

    return (
      <ul>
        { loading ? 'fetching...' : users.map((user) => <User key={user.id} user={user} />) }
      </ul>
    )
  }
}
