import React, { Component } from 'react'

export default class User extends Component {
  render () {
    const { email } = this.props.user

    return (
      <li>{email}</li>
    )
  }
}
