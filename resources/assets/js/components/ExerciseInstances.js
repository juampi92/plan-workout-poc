import React, { Component } from 'react'
import { Table } from 'reactstrap'

import AddExerciseInstance from '../Container/AddExerciseInstance'
import RemoveExerciseInstance from '../Container/RemoveExerciseInstance'

export default class ExerciseInstances extends Component {
  render () {
    const { pid, did, exercises } = this.props

    return (
      <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>Exercise</th>
            <th>Duration</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          { exercises.map((exercise, i) => this.renderExercise(exercise, i)) }
          <AddExerciseInstance pid={pid} did={did} />
        </tbody>
      </Table>
    )
  }

  renderExercise (exercise, i) {
    const { pid, did } = this.props

    return (<tr key={exercise.id}>
      <th scope='row'>{ i + 1 }</th>
      <td>{ exercise.name }</td>
      <td>{ exercise.duration }</td>
      <td>
        <RemoveExerciseInstance pid={pid} did={did} exercise={exercise} />
      </td>
    </tr>)
  }
}
