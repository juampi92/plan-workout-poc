import React, { Component } from 'react'

export default class Header extends Component {
  render () {
    return (
      <nav className='navbar navbar-dark bg-dark'>
        <div className='container'>
          <a className='navbar-brand' href='#'>VirtuaGYM</a>
        </div>
      </nav>
    )
  }
}
