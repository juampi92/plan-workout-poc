import React, { Component } from 'react'
import { Card, CardHeader, CardFooter, CardBody } from 'reactstrap'

import RemoveDay from '../Container/RemoveDay'

import ExerciseInstances from './ExerciseInstances'

export default class Day extends Component {
  render () {
    const { day, plan } = this.props
    const { name, exercises } = day

    return (
      <Card style={{ margin: 15 }} className='w-100'>
        <CardHeader>{ name }</CardHeader>
        <CardBody>
          <ExerciseInstances pid={plan.id} did={day.id} exercises={exercises} />
        </CardBody>
        <CardFooter>
          <RemoveDay pid={plan.id} day={day} />
        </CardFooter>
      </Card>
    )
  }
}
