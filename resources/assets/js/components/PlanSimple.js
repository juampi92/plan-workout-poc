import React, { Component } from 'react'
import { ListGroupItem, ListGroupItemText, ListGroupItemHeading } from 'reactstrap'

export default class PlanSimple extends Component {
  constructor (props) {
    super(props)

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (e) {
    e.preventDefault()
    const { name, id } = this.props.plan
    this.props.onPlanClick({ name, id })
  }

  render () {
    const { name, description } = this.props.plan

    return (
      <ListGroupItem tag='a' href='#' action
        onClick={this.handleClick}>
        <ListGroupItemHeading>{ name }</ListGroupItemHeading>
        <ListGroupItemText>
          { description }
        </ListGroupItemText>
      </ListGroupItem>
    )
  }
}
