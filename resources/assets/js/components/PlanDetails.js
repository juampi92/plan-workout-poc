import React, { Component } from 'react'

import Day from './Day'
import RemovePlan from '../Container/RemovePlan'

import AddDay from '../Container/AddDay'

export default class PlanDetails extends Component {
  constructor (props) {
    super(props)

    this.state = {
      deleteModal: false
    }
  }

  render () {
    const { plan } = this.props
    const { id: pid, description } = plan

    return (
      <div>
        <p className='font-italic'>{ description }</p>
        <div>
          <RemovePlan pid={pid} plan={plan} />
        </div>
        <div className='row'>
          { this.renderDays() }
        </div>
        <div>
          <AddDay pid={pid} />
        </div>
      </div>
    )
  }

  renderDays () {
    const { plan } = this.props
    const { days } = plan

    if (!days.length) {
      return (<div className='card w-100' style={{ margin: 15, padding: 10 }}>
        This plan has no days... Yet!
      </div>)
    }

    return days.map((day) => <Day key={day.id} day={day} plan={plan} />)
  }
}
