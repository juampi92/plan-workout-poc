import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

export default class ConfirmModal extends React.Component {
  render () {
    const {
      isOpen,
      title = 'Confirm',
      children = 'Are you sure you want to do that action?',
      className = '',
      onCancel, onConfirm,
      cancelText = 'Cancel',
      confirmText = 'Confirm',
      confirmColor = 'primaty'
      } = this.props

    return (
      <Modal isOpen={isOpen} className={className}>
        <ModalHeader toggle={this.toggle}>{ title }</ModalHeader>
        <ModalBody>
          { children }
        </ModalBody>
        <ModalFooter>
          <Button color={confirmColor} onClick={onConfirm}>{ confirmText }</Button>{' '}
          <Button color='secondary' onClick={onCancel}>{ cancelText }</Button>
        </ModalFooter>
      </Modal>
    )
  }
}
