import React, { Component } from 'react'
import { ListGroup } from 'reactstrap'

import Plan from './PlanSimple'

export default class Plans extends Component {
  render () {
    const { loading, plans } = this.props

    if (loading) {
      return null
    }

    return (
      <ListGroup>
        { plans.map((plan) =>
          <Plan key={plan.id}
            plan={plan}
            onPlanClick={this.props.onPlanClick} />)
        }
      </ListGroup>
    )
  }
}
