import { createReducer } from 'reduxsauce'
import Immutable from 'immutable'
import { PlanTypes } from '../actions/plan.actions'
import { DayTypes } from '../actions/day.actions'
import { ExerciseInstanceTypes } from '../actions/exerciseinstance.actions'
import { requestStarted, requestCompleted } from './shared.reducer'

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable.fromJS({
  fetching: true,
  planId: null,
  plan: null,
  editting: false,
  requestSucceed: false
})

/* ------------- Reducers ------------- */

const set = (state, { plan }) =>
  state.set('plan', Immutable.fromJS(plan))

const fetch = (state, { id, name }) =>
  state.set('planId', Immutable.fromJS({ id, name })).set('fetching', true)

const clear = (state) =>
  state.set('plan', null).set('planId', null)

const toggleEdit = (state) =>
  state.set('editting', !state.get('editting'))

/* ----------- Day Reducers ----------- */

const pushDay = (state, { pid, day }) => {
  if (!state.get('planId') || state.get('planId').toJS().id !== pid) {
    return state
  }

  const plan = state.get('plan').toJS()
  plan.days.push({ ...day, exercises: [] })

  return state.set('plan', Immutable.fromJS(plan))
}

const removeDay = (state, { pid, did }) => {
  if (!state.get('planId') || state.get('planId').toJS().id !== pid) {
    return state
  }

  const plan = state.get('plan').toJS()
  plan.days = plan.days.filter((d) => d.id !== did)

  return state.set('plan', Immutable.fromJS(plan))
}

const updateDay = (state, { pid, day }) => {
  console.log('updating day', { pid, day })
  return state
}

/* ------- Day Exercises Reducers ------ */

const pushDayExercise = (state, { pid, did, exercise }) => {
  if (!state.get('planId') || state.get('planId').toJS().id !== pid) {
    return state
  }

  const plan = state.get('plan').toJS()
  plan.days.find((d) => d.id === did).exercises.push(exercise)

  return state.set('plan', Immutable.fromJS(plan))
}

const removeDayExercise = (state, { pid, did, eid }) => {
  if (!state.get('planId') || state.get('planId').toJS().id !== pid) {
    return state
  }

  const plan = state.get('plan').toJS()
  let day = plan.days.find((d) => d.id === did)
  day.exercises = day.exercises.filter((e) => e.id !== eid)

  return state.set('plan', Immutable.fromJS(plan))
}

const updateDayExercise = (state, { pid, day }) => {
  console.log('updating day', { pid, day })
  return state
}

/* -------------------------------------------------------- */

export const reducer = createReducer(INITIAL_STATE, {
  // Plan reducers
  [PlanTypes.SET]: set,
  [PlanTypes.FETCH]: fetch,
  [PlanTypes.CLEAR]: clear,
  [PlanTypes.TOGGLE_EDIT]: toggleEdit,

  [PlanTypes.REQUEST_STARTED]: requestStarted,
  [PlanTypes.REQUEST_COMPLETED]: requestCompleted,

  // Days reducers
  [DayTypes.PUSH]: pushDay,
  [DayTypes.REMOVE]: removeDay,
  [DayTypes.UPDATE]: updateDay,

  [DayTypes.REQUEST_STARTED]: requestStarted,
  [DayTypes.REQUEST_COMPLETED]: requestCompleted,

  // Days Exercises reducers
  [ExerciseInstanceTypes.PUSH]: pushDayExercise,
  [ExerciseInstanceTypes.REMOVE]: removeDayExercise,
  [ExerciseInstanceTypes.UPDATE]: updateDayExercise,

  [ExerciseInstanceTypes.REQUEST_STARTED]: requestStarted,
  [ExerciseInstanceTypes.REQUEST_COMPLETED]: requestCompleted
})

export default reducer
