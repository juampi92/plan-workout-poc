import { createReducer } from 'reduxsauce'
import Immutable from 'immutable'
import { PlansTypes as Types } from '../actions/plans.actions'
import { requestStarted, requestCompleted } from './shared.reducer'

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable.fromJS({
  fetching: true,
  plans: Immutable.List(),
  requestSucceed: false
})

/* ------------- Reducers ------------- */

const set = (state, { plans }) =>
  state.set('plans', Immutable.fromJS(plans))

const push = (state, { plan }) =>
  state.set('plans', state.get('plans').push(plan))

const remove = (state, { id }) =>
  state.set('plans', state.get('plans').filter(o => o.get('id') !== id))

const update = (state, { plan }) =>
  state.get('plans').update(
    state.get('plans').findIndex((item) => item.get('id') === plan.id),
    (item) => Immutable.fromJS({ ...item.toJS(), plan })
  )

/* -------------------------------------------------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET]: set,
  [Types.PUSH]: push,
  [Types.REMOVE]: remove,
  [Types.UPDATE]: update,

  [Types.REQUEST_STARTED]: requestStarted,
  [Types.REQUEST_COMPLETED]: requestCompleted
})

export default reducer
