import { combineReducers } from 'redux'
import configureStore from './createStore'
import rootSaga from '../sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    plans: require('./plans.reducer').reducer,
    plan: require('./plan.reducer').reducer,
    exercises: require('./exercises.reducer').reducer
  })

  return configureStore(rootReducer, rootSaga)
}
