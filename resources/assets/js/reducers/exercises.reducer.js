import { createReducer } from 'reduxsauce'
import Immutable from 'immutable'
import { ExercisesTypes as Types } from '../actions/exercises.actions'
import { requestStarted, requestCompleted } from './shared.reducer'

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable.fromJS({
  fetching: true,
  exercises: Immutable.List(),
  requestSucceed: false
})

/* ------------- Reducers ------------- */

const set = (state, { exercises }) =>
  state.set('exercises', Immutable.fromJS(exercises))

/* -------------------------------------------------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET]: set,

  [Types.REQUEST_STARTED]: requestStarted,
  [Types.REQUEST_COMPLETED]: requestCompleted
})

export default reducer
