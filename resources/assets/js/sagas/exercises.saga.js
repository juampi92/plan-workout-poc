import { put, call } from 'redux-saga/effects'
import ExercisesActions from '../actions/exercises.actions'
import ErrorHandler from '../services/error_handler'

/**
 * Notify the plans that a request has started
 *
 * @export
 */
export function * started () {
  yield put(ExercisesActions.requestStarted())
}

/**
 * Notify the plans that the request was completed
 *
 * @export
 */
export function * completed (response) {
  yield put(ExercisesActions.requestCompleted())
}

/* ================= Plans ============= */

/**
 * Search plans
 *
 * @export
 * @param {API} api API service
 */
export function * getExercises (api) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.exercises)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    const exercises = response.data.data
      .map((e) => ({
        id: e.id,
        name: e.exercise_name
      }))

    yield put(ExercisesActions.set(exercises))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}
