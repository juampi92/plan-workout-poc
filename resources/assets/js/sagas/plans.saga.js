import { put, call } from 'redux-saga/effects'
import PlansActions from '../actions/plans.actions'
import ErrorHandler from '../services/error_handler'

/**
 * Notify the plans that a request has started
 *
 * @export
 */
export function * started () {
  yield put(PlansActions.requestStarted())
}

/**
 * Notify the plans that the request was completed
 *
 * @export
 */
export function * completed (response) {
  yield put(PlansActions.requestCompleted())
}

/* ================= Plans ============= */

/**
 * Search plans
 *
 * @export
 * @param {API} api API service
 */
export function * getPlans (api) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.plans)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield * successGetPlans(response)
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Handles the success
 *
 * @export
 * @param {object} response
 * @param {object} response.data
 */
export function * successGetPlans ({ data }) {
  // Stores the search results and set's it's sucess state
  yield put(PlansActions.set(data.data))
}

/**
 * Store
 */
export function * storePlan (api, { name, description, difficulty }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.createPlan, name, description, difficulty)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(PlansActions.push(response.data.data))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Update
 */
export function * updatePlan (api, { id, name, description, difficulty }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.updatePlan, id, name, description, difficulty)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(PlansActions.update(response.data.data))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Delete
 */
export function * deletePlan (api, { id }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.deletePlan, id)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(PlansActions.remove(id))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}
