import { put, call } from 'redux-saga/effects'
import DayActions from '../actions/day.actions'
import ErrorHandler from '../services/error_handler'

/**
 * Notify the plans that a request has started
 *
 * @export
 */
export function * started () {
  yield put(DayActions.requestStarted())
}

/**
 * Notify the plans that the request was completed
 *
 * @export
 */
export function * completed (response) {
  yield put(DayActions.requestCompleted())
}

/* ================= Days ============= */

/**
 * Store
 */
export function * storeDay (api, { pid, name }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.createDay, pid, name)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(DayActions.push(pid, response.data.data))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Update
 */
export function * updateDay (api, { pid, did, name }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.updateDay, pid, did, name)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(DayActions.update(pid, response.data.data))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Delete
 */
export function * deleteDay (api, { pid, did }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.deleteDay, pid, did)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(DayActions.remove(pid, did))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}
