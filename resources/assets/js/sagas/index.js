import { takeLatest } from 'redux-saga/effects'
import API from '../services/api'

/* ------------- Types ------------- */

import { PlansTypes } from '../actions/plans.actions'
import { PlanTypes } from '../actions/plan.actions'
import { DayTypes } from '../actions/day.actions'
import { ExercisesTypes } from '../actions/exercises.actions'
import { ExerciseInstanceTypes } from '../actions/exerciseinstance.actions'

/* ------------- Sagas ------------- */

import { getPlans, storePlan, updatePlan, deletePlan } from './plans.saga'
import { getPlan } from './plan.saga'
import { storeDay, updateDay, deleteDay } from './day.saga'
import { getExercises } from './exercises.saga'
import { storeDayExercise, updateDayExercise, deleteDayExercise } from './exerciseinstances.saga'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield [
    // Plans
    takeLatest(PlansTypes.FETCH, getPlans, api),
    takeLatest(PlansTypes.STORE, storePlan, api),
    takeLatest(PlansTypes.EDIT, updatePlan, api),
    takeLatest(PlansTypes.DELETE, deletePlan, api),

    // Plan
    takeLatest(PlanTypes.FETCH, getPlan, api),

    // Days
    takeLatest(DayTypes.STORE, storeDay, api),
    takeLatest(DayTypes.EDIT, updateDay, api),
    takeLatest(DayTypes.DELETE, deleteDay, api),

    // Exercises
    takeLatest(ExercisesTypes.FETCH, getExercises, api),

    // Day Exercise
    takeLatest(ExerciseInstanceTypes.STORE, storeDayExercise, api),
    takeLatest(ExerciseInstanceTypes.EDIT, updateDayExercise, api),
    takeLatest(ExerciseInstanceTypes.DELETE, deleteDayExercise, api)
  ]
}
