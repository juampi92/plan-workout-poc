import { put, call } from 'redux-saga/effects'
import PlanActions from '../actions/plan.actions'
import ErrorHandler from '../services/error_handler'

/**
 * Notify the plans that a request has started
 *
 * @export
 */
export function * started () {
  yield put(PlanActions.requestStarted())
}

/**
 * Notify the plans that the request was completed
 *
 * @export
 */
export function * completed (response) {
  yield put(PlanActions.requestCompleted())
}

/* ================= Plans ============= */

/**
 * Search plans
 *
 * @export
 * @param {API} api API service
 */
export function * getPlan (api, { id }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.plan, id)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield * successGetPlan(response)
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Handles the success
 *
 * @export
 * @param {object} response
 * @param {object} response.data
 */
export function * successGetPlan ({ data }) {
  yield put(PlanActions.set(data.data))
}
