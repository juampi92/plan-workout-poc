import { put, call } from 'redux-saga/effects'
import ExerciseInstanceActions from '../actions/exerciseinstance.actions'
import ErrorHandler from '../services/error_handler'

/**
 * Notify the plans that a request has started
 *
 * @export
 */
export function * started () {
  yield put(ExerciseInstanceActions.requestStarted())
}

/**
 * Notify the plans that the request was completed
 *
 * @export
 */
export function * completed (response) {
  yield put(ExerciseInstanceActions.requestCompleted())
}

/* ================= Days ============= */

/**
 * Store
 */
export function * storeDayExercise (api, { pid, did, exercise_id, duration }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.createDayExercise, did, exercise_id, duration)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(ExerciseInstanceActions.push(pid, did, response.data.data))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Update
 */
export function * updateDayExercise (api, { pid, did, eid, exercise_id, duration }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.updateDayExercise, did, eid, exercise_id, duration)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(ExerciseInstanceActions.update(pid, did, response.data.data))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}

/**
 * Delete
 */
export function * deleteDayExercise (api, { pid, did, eid }) {
  // Notify start
  yield * started()
  // Call api
  const response = yield call(api.deleteDayExercise, did, eid)
  // Notify completed
  yield * completed()

  if (response.ok) {
    // Handle success
    yield put(ExerciseInstanceActions.remove(pid, did, eid))
  } else {
    // Handle error
    yield * ErrorHandler(response)
  }
}
