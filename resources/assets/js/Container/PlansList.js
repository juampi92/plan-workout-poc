import React, { Component } from 'react'
import { ListGroup } from 'reactstrap'

import { connect } from 'react-redux'

import PlansActions from '../actions/plans.actions'
import PlanActions from '../actions/plan.actions'

import Plan from '../components/PlanSimple'

class PlansList extends Component {
  componentDidMount () {
    this.props.fetchAll()
  }

  render () {
    return (
      <div>
        { this.renderList() }
      </div>
    )
  }

  renderList () {
    const { fetching, plans } = this.props

    if (!plans.length) {
      return fetching ? null : (<div>Empty</div>)
    }

    return (
      <ListGroup>
        { plans.map((plan) =>
          <Plan key={plan.id}
            plan={plan}
            onPlanClick={this.props.fetch.bind(this, plan)} />)
        }
      </ListGroup>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  fetchAll: () => { dispatch(PlansActions.fetch()) },
  fetch: ({id, name}) => { dispatch(PlanActions.fetch(id, name)) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  fetching: state.plans.get('fetching'),
  plans: state.plans.get('plans').toJS()
})

export default connect(mapStateToProps, mapDispatchToProps)(PlansList)
