import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input, Card, CardBody } from 'reactstrap'

import { connect } from 'react-redux'

import DayActions from '../actions/day.actions'

class AddDay extends Component {
  constructor (props) {
    super(props)

    this.state = this.restart()

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  restart () {
    return {
      hidden: true,
      name: ''
    }
  }

  handleInputChange (event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit (e) {
    e.preventDefault()

    const { name } = this.state
    const { pid } = this.props

    this.props.store(pid, { name })
  }

  render () {
    const { hidden, name } = this.state
    const { saving } = this.props

    if (hidden) {
      return (<Button color='primary'
        onClick={() => this.setState({ hidden: false })}
        className='w-100'
        style={{ marginTop: 15 }}>+ Add day</Button>)
    }

    return (
      <Card style={{ marginTop: 15 }} className='w-100'>
        <CardBody>
          <Form onSubmit={this.handleSubmit}>
            <FormGroup>
              <Label>Day's name</Label>
              <Input type='text' value={name} name='name' className='form-control'
                onChange={this.handleInputChange}
                aria-describedby='dayName' placeholder="Enter day's name"
                disabled={saving} />
            </FormGroup>
            <Button type='submit' color='primary' disabled={saving}>Add Day</Button>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  store: (pid, day) => { dispatch(DayActions.store(pid, day)) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  ...ownProps,
  saving: state.plans.get('fetching')
})

export default connect(mapStateToProps, mapDispatchToProps)(AddDay)
