import React, { Component } from 'react'

import { connect } from 'react-redux'

import ExerciseInstanceActions from '../actions/exerciseinstance.actions'

class RemoveExerciseInstance extends Component {
  constructor (props) {
    super(props)

    this.state = { deleteModal: false }

    this.handleDelete = this.handleDelete.bind(this)
  }

  handleDelete (e) {
    e.preventDefault()

    const { pid, did, exercise } = this.props

    this.props.delete(pid, did, exercise.id)
  }

  render () {
    return (<a className='text-danger' onClick={this.handleDelete}>delete</a>)
  }
}

const mapDispatchToProps = dispatch => ({
  delete: (pid, did, eid) => { dispatch(ExerciseInstanceActions.delete(pid, did, eid)) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  ...ownProps
})

export default connect(mapStateToProps, mapDispatchToProps)(RemoveExerciseInstance)
