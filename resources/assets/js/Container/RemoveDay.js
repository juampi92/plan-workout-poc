import React, { Component } from 'react'

import { connect } from 'react-redux'

import DayActions from '../actions/day.actions'

import ConfirmModal from '../components/Modals/ConfirmModal'

class RemoveDay extends Component {
  constructor (props) {
    super(props)

    this.state = { deleteModal: false }
  }

  render () {
    const { pid, day } = this.props

    return (<div>
      <a href='#' className='text-danger float-right' onClick={() => this.setState({ deleteModal: true })}>
        Delete day
      </a>
      <ConfirmModal
        isOpen={this.state.deleteModal}
        onCancel={() => this.setState({ deleteModal: false })}
        onConfirm={() => {
          this.props.delete(pid, day.id)
          this.setState({ deleteModal: false })
        }}
        confirmColor='danger'>
        Are you sure you want to delete the day { day.name }?
      </ConfirmModal>
    </div>)
  }
}

const mapDispatchToProps = dispatch => ({
  delete: (pid, did) => { dispatch(DayActions.delete(pid, did)) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  ...ownProps,
  saving: state.plans.get('fetching')
})

export default connect(mapStateToProps, mapDispatchToProps)(RemoveDay)
