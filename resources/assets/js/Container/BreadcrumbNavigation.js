import React, { Component } from 'react'
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'

import { connect } from 'react-redux'

import PlanActions from '../actions/plan.actions'

class BreadcrumbNavigation extends Component {
  render () {
    return (
      <Breadcrumb>
        { this.renderHome() }
        { this.renderPlan() }
      </Breadcrumb>
    )
  }

  renderHome () {
    const { plan, back } = this.props

    if (plan) {
      return (<BreadcrumbItem><a href='#' onClick={back}>Home</a></BreadcrumbItem>)
    }

    return (<BreadcrumbItem active>Home</BreadcrumbItem>)
  }

  renderPlan () {
    const { plan } = this.props

    if (!plan) return null

    return (<BreadcrumbItem active>{ plan.name }</BreadcrumbItem>)
  }
}

const mapDispatchToProps = dispatch => ({
  back: () => { dispatch(PlanActions.clear()) }
})

const mapStateToProps = (state, ownProps = {}) => {
  const plan = state.plan.get('planId')
  return {
    plan: plan ? plan.toJS() : null
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BreadcrumbNavigation)
