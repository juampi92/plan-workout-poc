import React, { Component } from 'react'
import { Button, FormGroup, Input } from 'reactstrap'

import { connect } from 'react-redux'

import ExerciseSelect from './ExerciseSelect'

import ExerciseInstancesActions from '../actions/exerciseinstance.actions'

class AddExerciseInstance extends Component {
  constructor (props) {
    super(props)

    this.state = this.restart()

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  restart () {
    return {
      exerciseId: '',
      duration: 100,
      order: 99
    }
  }

  handleInputChange (event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit (e) {
    e.preventDefault()

    const { exerciseId, duration, order } = this.state
    const { store, pid, did } = this.props

    store(pid, did, { exercise_id: exerciseId, duration, order })
  }

  render () {
    const { exerciseId, duration } = this.state
    const { saving } = this.props

    return (<tr>
      <td>#</td>
      <td>
        <FormGroup>
          <ExerciseSelect value={exerciseId} name='exerciseId' onChange={this.handleInputChange} />
        </FormGroup>
      </td>
      <td>
        <FormGroup>
          <Input type='number' value={duration} name='duration'
            onChange={this.handleInputChange}
            disabled={saving} />
        </FormGroup>
      </td>
      <td>
        <Button type='submit' color='primary' onClick={this.handleSubmit} disabled={saving}>Add</Button>
      </td>
    </tr>)
  }
}

const mapDispatchToProps = dispatch => ({
  store: (pid, did, exercise) => { dispatch(ExerciseInstancesActions.store(pid, did, exercise)) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  ...ownProps
})

export default connect(mapStateToProps, mapDispatchToProps)(AddExerciseInstance)
