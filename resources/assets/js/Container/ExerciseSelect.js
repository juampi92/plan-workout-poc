import React, { Component } from 'react'
import { Input } from 'reactstrap'

import { connect } from 'react-redux'

class ExerciseSelect extends Component {
  render () {
    const { exercises, ...props } = this.props

    // Remove dispatch prop
    delete props.dispatch

    return (<Input type='select' {...props}>
      <option key='_' value=''>- Select -</option>
      { exercises.map((e) => (<option key={e.id} value={e.id}>{e.name}</option>)) }
    </Input>)
  }
}

const mapStateToProps = (state, ownProps = {}) => ({
  ...ownProps,
  exercises: state.exercises.get('exercises').toJS()
})

export default connect(mapStateToProps)(ExerciseSelect)
