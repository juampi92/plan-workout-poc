import React, { Component } from 'react'
import { Button } from 'reactstrap'

import { connect } from 'react-redux'

import PlansActions from '../actions/plans.actions'
import PlanActions from '../actions/plan.actions'

import ConfirmModal from '../components/Modals/ConfirmModal'

class RemovePlan extends Component {
  constructor (props) {
    super(props)

    this.state = { deleteModal: false }
  }

  render () {
    const { pid, plan } = this.props

    return (<div>
      <Button color='danger' onClick={() => this.setState({ deleteModal: true })}>
        Delete plan
      </Button>
      <ConfirmModal
        isOpen={this.state.deleteModal}
        onCancel={() => this.setState({ deleteModal: false })}
        onConfirm={() => {
          this.props.delete(pid)
          this.setState({ deleteModal: false })
        }}
        confirmColor='danger'>
        Are you sure you want to delete the plan { plan.name } and all it's days and exercises?
      </ConfirmModal>
    </div>)
  }
}

const mapDispatchToProps = dispatch => ({
  delete: (id) => {
    dispatch(PlansActions.delete(id))
    dispatch(PlanActions.clear())
  }
})

const mapStateToProps = (state, ownProps = {}) => ({
  ...ownProps
})

export default connect(mapStateToProps, mapDispatchToProps)(RemovePlan)
