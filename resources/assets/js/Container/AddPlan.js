import React, { Component } from 'react'
import { Button, Form, FormGroup, Label, Input, Card, CardBody } from 'reactstrap'

import { connect } from 'react-redux'

import PlansActions from '../actions/plans.actions'

class AddPlan extends Component {
  constructor (props) {
    super(props)

    this.state = this.restart()

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  restart () {
    return {
      hidden: true,
      name: '',
      description: '',
      difficulty: '1'
    }
  }

  handleInputChange (event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit (e) {
    e.preventDefault()

    const { name, description, difficulty } = this.state

    this.props.store({ name, description, difficulty })
  }

  render () {
    const { hidden, name, description, difficulty } = this.state
    const { saving } = this.props

    if (hidden) {
      return (<button onClick={() => this.setState({ hidden: false })}
        className='btn btn-primary'>Add plan</button>)
    }

    return (
      <Card>
        <CardBody>
          <Form onSubmit={this.handleSubmit} className='well'>
            <FormGroup>
              <Label>Plan's name</Label>
              <Input type='text' value={name} name='name' className='form-control'
                onChange={this.handleInputChange}
                aria-describedby='planName' placeholder="Enter plan's name"
                disabled={saving} />
            </FormGroup>
            <FormGroup>
              <Label>Description</Label>
              <textarea value={description} name='description'
                onChange={this.handleInputChange}
                className='form-control'
                disabled={saving} />
            </FormGroup>
            <FormGroup>
              <div className='form-check form-check-inline'>
                <Input className='form-check-input' type='radio' name='difficulty' id='difficulty1'
                  value='1' checked={difficulty === '1'} onChange={this.handleInputChange} />
                <Label className='form-check-label' htmlFor='difficulty1'>Beginner</Label>
              </div>
              <div className='form-check form-check-inline'>
                <Input className='form-check-input' type='radio' name='difficulty' id='difficulty2'
                  value='2' checked={difficulty === '2'} onChange={this.handleInputChange} />
                <Label className='form-check-label' htmlFor='difficulty2'>Intermediate</Label>
              </div>
              <div className='form-check form-check-inline'>
                <Input className='form-check-input' type='radio' name='difficulty' id='difficulty3'
                  value='3' checked={difficulty === '3'} onChange={this.handleInputChange} />
                <Label className='form-check-label' htmlFor='difficulty3'>Expert</Label>
              </div>
            </FormGroup>
            <Button type='submit' color='primary' disabled={saving}>Submit</Button>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  store: (plan) => { dispatch(PlansActions.store(plan)) }
})

const mapStateToProps = (state, ownProps = {}) => ({
  saving: state.plans.get('fetching')
})

export default connect(mapStateToProps, mapDispatchToProps)(AddPlan)
