
/**
 * Parses the message or messages into a formatted string
 *
 * @param {string|array} message
 * @returns {string}
 */
function parseMessages (message) {
  return typeof message === 'string' ? message : message.join('\n')
}

/**
 * Handle the errors (it's de default export)
 *
 * @export
 * @param {object} response API response object
 * @returns {null}
 */
export default function * handle (response) {
  if (response.ok) return
  console.error('response in error handler => ', response)

  switch (response.problem) {
    case 'CLIENT_ERROR':
    case 'SERVER_ERROR':
      yield * appErrors(response)
      break
    case 'NETWORK_ERROR':
    case 'CONNECTION_ERROR':
    case 'TIMEOUT_ERROR':
      showMessage('There were some problems connecting to the server.\nTry again later.')
      break
    default:
      showMessage('Something happened\nTry again later')
      break
  }
}

/**
 * Handles the specific app errors
 *
 * @private
 * @param {object} response
 * @returns {null}
 */
function * appErrors (response) {
  if (response.data === null || !response.data.error) {
    showMessage('Something happened\nTry again later')
    return
  }
  const {data: {error: {name, message}}} = response

  switch (name) {
    case 'InvalidFields':
      showMessage('The following errors ocurred:\n' + parseMessages(message))
      break
    default:
      if (message) showMessage('The following errors ocurred:\n' + parseMessages(message))
  }
}

function showMessage (message) {
  window.alert(message)
}
