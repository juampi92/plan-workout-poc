import apisauce from 'apisauce'

const BASE_URL = 'api/v1'

const create = (baseURL = BASE_URL) => {
  /* ----------- Apisauce ----------- */

  const api = apisauce.create({
    baseURL,
    // Default headers
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json'
    },
    // Requests timeout
    timeout: 10000
  })

  /* ----------- Attach debugger ----------- */
  api.addMonitor(response => console.log('[API] response => ', response))

  /* ----------- API Methods ----------- */

  // Users
  const users = () =>
    api.get('users')

  const createUser = ({ first_name, last_name, email }) =>
    api.post('users', { first_name, last_name, email })

  const editUser = (id, { first_name, last_name }) =>
    api.put(`users/${id}`, { first_name, last_name })

  const deleteUser = (id) =>
    api.delete(`users/${id}`)

  const user = (id) =>
    api.get(`users/${id}`)

  const userPlans = (id) =>
    api.get(`users/${id}/plans`)

  const userPlanJoin = (uid, pid) =>
    api.get(`users/${uid}/plans/${pid}/join`)

  const userPlanLeave = (uid, pid) =>
    api.get(`users/${uid}/plans/${pid}/leave`)

  // Plans
  const plans = () =>
    api.get('plans')

  const createPlan = ({ name, description, difficulty }) =>
    api.post('plans', { plan_name: name, plan_description: description, plan_difficulty: difficulty })

  const updatePlan = (id, { name, description, difficulty }) =>
    api.put(`plans/${id}`, { plan_name: name, plan_description: description, plan_difficulty: difficulty })

  const deletePlan = (id) =>
    api.delete(`plans/${id}`)

  const plan = (id) =>
    api.get(`plans/${id}`)

  // Days
  const planDays = (id) =>
    api.get(`plans/${id}/days`)

  const createDay = (pid, { name, order }) =>
    api.post(`plans/${pid}/days`, { day_name: name, order })

  const updateDay = (pid, did, { name, order }) =>
    api.update(`plans/${pid}/days/${did}`, { day_name: name, order })

  const deleteDay = (pid, did) =>
    api.delete(`plans/${pid}/days/${did}`)

  // Exercises

  const exercises = () =>
  api.get('exercises')

  const createExercise = ({ name }) =>
    api.post('exercises', { name })

  const editExercise = (id, { name }) =>
    api.put(`exercises/${id}`, { name })

  const deleteExercise = (id) =>
    api.delete(`exercises/${id}`)

  const exercise = (id) =>
    api.get(`exercises/${id}`)

  // Exercise Instances

  const createDayExercise = (did, { exercise_id, duration }) =>
    api.post(`days/${did}/exercises`, { exercise_id, exercise_duration: duration })

  const updateDayExercise = (did, eid, { exercise_id, duration }) =>
    api.update(`days/${did}/exercises/${eid}`, { exercise_id, exercise_duration: duration })

  const deleteDayExercise = (did, eid) =>
    api.delete(`days/${did}/exercises/${eid}`)

  /* ----------- Expose methods ----------- */

  return {
    // Users
    users,
    createUser,
    editUser,
    deleteUser,
    user,
    userPlans,
    userPlanJoin,
    userPlanLeave,
    // Plans
    plans,
    createPlan,
    updatePlan,
    deletePlan,
    plan,
    // Days
    planDays,
    createDay,
    updateDay,
    deleteDay,
    // Exercises
    exercises,
    createExercise,
    editExercise,
    deleteExercise,
    exercise,
    // Exercise Instances
    createDayExercise,
    updateDayExercise,
    deleteDayExercise
  }
}

export default {
  create
}
