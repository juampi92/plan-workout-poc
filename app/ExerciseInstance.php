<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseInstance extends Model
{
    protected $table = 'exercise_instances';

    public $timestamps = false;

    protected $fillable = ['exercise_id', 'exercise_duration', 'order'];

    /**
     * Relationships
     */

    public function exercise_name()
    {
        return $this->belongsTo(Exercise::class, 'exercise_id', 'id');
    }
}
