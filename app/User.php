<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    protected $fillable = ['first_name', 'last_name', 'email'];

    public function plans()
    {
        return $this->belongsToMany('App\Plan');
    }
}
