<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $table = 'exercise';

    protected $fillable = ['exercise_name'];

    public $timestamps = false;

    /**
     * Relationships
     */
}
