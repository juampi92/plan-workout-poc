<?php

namespace App\Repositories;

use App\ExerciseInstance;
use App\Plan;
use App\PlanDay;

class PlanRemovalRepository
{

    /**
     * @param PlanDay $day
     *
     * @return mixed
     */
    public function removeExerciseInstances(PlanDay $day)
    {
        return ExerciseInstance::where('day_id', $day->id)->delete();
    }

    /**
     * @param PlanDay $day
     *
     * @return bool|null
     */
    public function removeDay(PlanDay $day)
    {
        $this->removeExerciseInstances($day);

        return $day->delete();
    }

    /**
     * @param Plan $plan
     *
     * @return int
     */
    public function removeDays(Plan $plan)
    {
        $dayIds = $plan->days->pluck('id')->all();

        ExerciseInstance::whereIn('day_id', $dayIds)
            ->delete();

        return PlanDay::destroy($dayIds);
    }

    /**
     * @param Plan $plan
     *
     * @return mixed
     */
    public function removePlan(Plan $plan)
    {
        $this->removeDays($plan);
        return $plan->delete();
    }
}
