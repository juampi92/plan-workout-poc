<?php

namespace App\Http\Controllers\Api;

use App\ExerciseInstance;
use App\Http\Controllers\Controller;
use App\Http\Resources\ExerciseInstance as ExerciseInstanceResource;
use App\PlanDay;
use Illuminate\Http\Request;

class ExerciseInstanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int $dayId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($dayId)
    {
        // TODO: list all exercise instances that belong to that day
        $exercises = ExerciseInstance::where('day_id', $dayId)
            ->select(['exercise_instances.id as id', 'exercise_instances.*', 'exercise.exercise_name'])
            ->leftJoin('exercise', 'exercise.id', '=', 'exercise_instances.exercise_id')
            ->orderBy('exercise_instances.order')
            ->get();

        return ExerciseInstanceResource::collection($exercises)->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int     $dayId
     * @param  Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $dayId)
    {
        $attrs = $request->validate([
            'exercise_id'       => 'required|exists:exercise,id',
            'exercise_duration' => 'required|numeric',
            'order'             => 'numeric',
        ]);

        // Set default order as 99
        $attrs['order'] = $request->input('order', 99);

        $day = PlanDay::findOrFail($dayId);
        $exercise_instance = new ExerciseInstance($attrs);

        $day->exercise_instances()->save($exercise_instance);

        return ExerciseInstanceResource::make($exercise_instance)->response();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $dayId
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($dayId, $id)
    {
        $exercise = ExerciseInstance::where('day_id', $dayId)
            ->select(['exercise_instances.id as id', 'exercise_instances.*', 'exercise.exercise_name'])
            ->leftJoin('exercise', 'exercise.id', '=', 'exercise_instances.exercise_id')
            ->find($id);

        return ExerciseInstanceResource::make($exercise)->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int     $dayId
     * @param  int     $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $dayId, $id)
    {
        $attrs = $request->validate([
            'exercise_id'       => 'exists:exercise,id',
            'exercise_duration' => 'numeric',
            'order'             => 'numeric',
        ]);

        $exercise_instance = ExerciseInstance::where('day_id', $dayId)
            ->find($id);

        $exercise_instance->fill($attrs);
        $exercise_instance->save();

        return ExerciseInstanceResource::make($exercise_instance)->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $dayId
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($dayId, $id)
    {
        ExerciseInstance::where('day_id', $dayId)
            ->where('id', $id)
            ->delete();

        return response()->json(['data' => 'success']);
    }
}
