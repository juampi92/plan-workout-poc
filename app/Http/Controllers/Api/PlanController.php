<?php

namespace App\Http\Controllers\Api;

use App\Notifications\PlanDeleted;
use App\Notifications\PlanUpdated;
use App\Plan;
use App\Http\Resources\Plan as PlanResource;
use App\Repositories\PlanRemovalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $plans = Plan::all();
        return PlanResource::collection($plans)->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $attrs = $request->validate([
            'plan_name'        => 'required|string|max:150',
            'plan_description' => 'required|string',
            'plan_difficulty'  => 'required|numeric|in:1,2,3',
        ]);

        $plan = new Plan($attrs);
        $plan->save();

        return (new PlanResource($plan))
            ->response();
    }

    /**
     * Display the specified resource.
     *
     * @param  Plan $plan
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Plan $plan)
    {
        // Also fetch relationships

        $days = $plan->days()
            ->with('exercises')
            ->get();

        $plan->setRelation('days', $days);

        return (new PlanResource($plan))
            ->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Plan                      $plan
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function update(Request $request, Plan $plan)
    {
        $attrs = $request->validate([
            'plan_name'        => 'string|max:150',
            'plan_description' => 'string',
            'plan_difficulty'  => 'numeric|in:1,2,3',
        ]);

        $plan->fill($attrs);
        $plan->update();

        // Notify users that it has updated
        $plan->notify(new PlanUpdated());

        return (new PlanResource($plan))
            ->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Plan                  $plan
     * @param PlanRemovalRepository $repo
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $plan, PlanRemovalRepository $repo)
    {
        $repo->removePlan($plan);

        // Notify via Mail
        $plan->notify(new PlanDeleted());

        return response()->json(['data' => 'success']);
    }
}
