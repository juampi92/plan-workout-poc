<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Plan as PlanResource;

class UserPlanController extends Controller
{
    /**
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(User $user)
    {
        // Requires User in case it's invalid

        return PlanResource::collection($user->plans)
            ->response();
    }

    /**
     * @param User $user
     * @param      $plan_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function join(User $user, $plan_id)
    {
        // Database checks if duplicate or non-existent foreign key
        $user->plans()->attach($plan_id);

        return response()->json(['data' => 'success']);
    }

    /**
     * @param User $user
     * @param      $plan_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function leave(User $user, $plan_id)
    {
        // Database checks if exists
        $user->plans()->detach($plan_id);

        return response()->json(['data' => 'success']);
    }
}
