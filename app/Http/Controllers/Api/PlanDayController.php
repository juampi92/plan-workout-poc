<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PlanDay as PlanDayResource;
use App\Plan;
use App\PlanDay;
use App\Repositories\PlanRemovalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $planId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($planId)
    {
        $planDays = PlanDay::where('plan_id', $planId)
            ->with('exercises')
            ->get();

        return PlanDayResource::collection($planDays)
            ->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $planId)
    {
        $attrs = $request->validate([
            'day_name' => 'required|string|max:100',
            'order'    => 'numeric',
        ]);

        // Set default order as 99
        $attrs['order'] = $request->input('order', 99);

        $plan = Plan::findOrFail($planId);

        $planDay = new PlanDay($attrs);

        $plan->days()->save($planDay);

        return (new PlanDayResource($planDay))
            ->response();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($planId, $id)
    {
        $planDay = PlanDay::where('plan_id', $planId)
            ->with('exercises')
            ->findOrFail($id);

        return (new PlanDayResource($planDay))
            ->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $planId
     * @param  int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $planId, $id)
    {
        $attrs = $request->validate([
            'day_name' => 'string|max:100',
            'order'    => 'numeric',
        ]);

        $planDay = PlanDay::where('plan_id', $planId)
            ->findOrFail($id);

        $planDay->fill($attrs);
        $planDay->update();

        return (new PlanDayResource($planDay))
            ->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                  $planId
     * @param  int                  $id
     * @param PlanRemovalRepository $repo
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($planId, $id, PlanRemovalRepository $repo)
    {
        $planDay = PlanDay::where('plan_id', $planId)
            ->findOrFail($id);

        $repo->removeDay($planDay);

        return response()->json(['data' => 'success']);
    }
}
