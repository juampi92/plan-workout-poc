<?php

namespace App\Http\Controllers\Api;

use App\Exercise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $exercises = Exercise::all();

        return response()
            ->json(['data' => $exercises]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $attrs = $request->validate([
            'name' => 'required|string|max:100',
        ]);

        $exercise = new Exercise($attrs);

        return response()
            ->json(['data' => $exercise]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exercise $exercise
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Exercise $exercise)
    {
        return response()
            ->json(['data' => $exercise]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Exercise            $exercise
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Exercise $exercise)
    {
        $attrs = $request->validate([
            'name' => 'required|string|max:100',
        ]);

        $exercise->fill($attrs);
        $exercise->save();

        return response()
            ->json(['data' => $exercise]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exercise $exercise
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Exercise $exercise)
    {
        $exercise->delete();

        return response()
            ->json(['data' => 'success']);
    }
}
