<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ExerciseInstance extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name(),
            'exercise_id' => $this->exercise_id,
            'duration'    => $this->exercise_duration,
            'order'       => $this->order,
        ];
    }

    protected function name() {
        if ($this->resource->getAttributeValue('exercise_name')) {
            return $this->exercise_name;
        }

        return $this->exercise_name->exercise_name;
    }
}
