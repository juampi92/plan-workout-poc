<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PlanDay extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->day_name,
            'exercises' => ExerciseInstance::collection($this->whenLoaded('exercises')),
        ];
    }
}
