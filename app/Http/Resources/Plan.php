<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Plan extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->plan_name,
            'description' => $this->plan_description,
            'difficulty'  => $this->plan_difficulty,
            'days'        => PlanDay::collection($this->whenLoaded('days')),
        ];
    }
}
