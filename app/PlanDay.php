<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDay extends Model
{
    public $timestamps = false;

    protected $fillable = ['day_name', 'order'];

    /**
     * Relationships
     */

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function exercises()
    {
        // Eloquent performs an Inner Join between exercise and exercise_instances
        return $this->hasManyThrough(Exercise::class,
            ExerciseInstance::class,
            'day_id',
            'id',
            'id',
            'exercise_id')
            ->select(['exercise.exercise_name', 'exercise_instances.*'])
            ->orderBy('exercise_instances.order');
    }

    public function exercise_instances()
    {
        return $this->hasMany(ExerciseInstance::class, 'day_id', 'id');
    }
}
