<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Plan extends Model
{
    use Notifiable;

    /**
     * Prevent it from auto-generating plans.
     *
     * @var string
     */
    protected $table = 'plan';

    protected $fillable = ['plan_name', 'plan_description', 'plan_difficulty'];

    public $timestamps = false;

    /**
     * Relationships
     */

    public function days()
    {
        return $this->hasMany('App\PlanDay', 'plan_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }


    /**
     * @return array
     */
    public function getMails()
    {
        return $this->users()
            ->select('email')
            ->get()
            ->pluck('email')
            ->all();
    }

    /**
     * Route notifications for the mail channel.
     *
     * @return array
     */
    public function routeNotificationForMail()
    {
        return $this->getMails();
    }
}
