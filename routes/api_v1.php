<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Exercises

Route::resource('exercises', 'ExerciseController', ['except' => ['create', 'edit']]);

// Users

Route::resource('users', 'UserController', ['except' => ['create', 'edit']]);

// Users - Plans

Route::get('users/{user}/plans', 'UserPlanController@index')
    ->name('users.plans');
Route::get('users/{user}/plans/{plan_id}/join', 'UserPlanController@join')
    ->name('users.plans.join');
Route::get('users/{user}/plans/{plan_id}/leave', 'UserPlanController@leave')
    ->name('users.plans.leave');

// Plans

Route::resource('plans', 'PlanController', ['except' => ['create', 'edit']]);

// Plans - Days

Route::resource('plans/{planId}/days', 'PlanDayController', ['except' => ['create', 'edit']]);

// Days - Exercises (ExerciseInstances)

Route::resource('days/{dayId}/exercises', 'ExerciseInstanceController', ['except' => ['create', 'edit']]);

