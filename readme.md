# Plan Workout PoC

## Install

### Server

Run `composer install` to install dependencies

Run `example_database.sql` on your database

Copy `.evn.example` to `.env` and configure it with the right database

Run `php artisan key:generate` to generate app key

Run `php artisan migrate` to install the rest of the database tables

Run `php artisan db:seed` to create some dummy records

### Front end

Run `yarn` to install npm dependencies

Run `npm run dev` to compile frontend assets (dev to be able to debug)

### Finally

Run `php artisan serve` to run the app on port 8000, or use LAMP on port 80.

Head to localhost:8000 to see it in action

## To-do

- Editing order
- Some front-end functionality like CRUD users, join/leave users from plans (out of scope)
- Front-end routing (out of scope)
- Front-end edit plans is missing (backend is complete)
- Some kind of authentication (out of scope)
- Front end toastr (using alert for displaying messages)

## Uses

- Uses Laravel 5.5
- React 15.4
- Redux
- Redux-saga with apisauce
- Bootstrap 4 with reactstrap

## Help

Run `php artisan route:list` to see all available endpoints

